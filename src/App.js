import logo from './logo.svg';
import Employees from './components/employees';
import './assets/scss/main.scss';

function App() {
  return (
    <div className="App">
      <Employees />
    </div>
  );
}

export default App;
