import axios from 'axios';

const fetchEmployees = async () => {
    try {
        const { data } = await axios.get("http://dummy.restapiexample.com/api/v1/employees");
        return data;
    } catch (error) {
        console.log('error', error);
        return [];
    }
}

export default fetchEmployees;