import React from 'react';

const Table = (props) => {
    const renderEmployees = () => {
        return props.list.map((item) => {
            return (
                <tr key={`employee_${item.id}`}>
                    <th scope="row">{item.id}</th>
                    <td>{item.employee_name}</td>
                    <td>{item.employee_age}</td>
                    <td>{item.employee_salary}</td>
                </tr>
            )
        });
    }

    return (
        <table className="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Age</th>
                    <th scope="col">Income</th>
                </tr>
            </thead>
            <tbody>
                {renderEmployees()}
            </tbody>
        </table>
    )
}

export default Table;